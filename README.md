# Attendance

**Group Nahong**

6510405369 จันทร์ทิพ พิริยบรรเจิด  
6410401078 ธนฤกษ์  โอเจริญ  
6410406771 ปัณณทัต  ด้วงแค  

---

## Environment

python\==3.7.11
tensorflow\==2.4.1  
imgaug  
scikit-image\==0.16.2  
labelme2coco\==0.1.2  
pixellib
matplotlib
opencv
natsort
numpy

## How to run

1. รันไฟล์ **generate_data_from_video.ipynb** เพื่อนำวีดิโอมาแปลงเป็นรูปภาพ
2. รันไฟล์ **train_custom_label.ipynb** เพื่อสร้าง Model
3. รันไฟล์ **model_test_image_video.ipynb** ทดสอบโดยการเปิดกล้อง

## ความคืบหน้า

มีการแปลงวีดิโอเป็นภาพเพื่อความสะดวกในการเก็บข้อมูล แล้วมีการสร้างโมเดลทดสอบจากคน 2 คนเพื่อทดลองโมเดล และมีการทดสอบในการเปิดกล้องเพื่อ Detect หน้าแล้ว Classify หน้าของบุคคล
